call plug#begin('~/.vim/plugged')
Plug 'Valloric/YouCompleteMe', { 'do': './install.py --tern-completer' }
Plug 'dense-analysis/ale'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'stefandtw/quickfix-reflector.vim'
Plug 'BurntSushi/ripgrep'
Plug 'janko/vim-test'
call plug#end()

set nocompatible
syntax on
set nowrap
set encoding=utf8
set lazyredraw

" Show linenumbers
set number
set ruler

" Set Proper Tabs
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab

" Set search
set hlsearch
set incsearch
" Theme and Styling
colorscheme gruvbox
set t_Co=256
set background=dark

" YouCompleteMe configuration
let g:ycm_min_num_of_chars_for_completion = 4
let g:ycm_min_num_identifier_candidate_chars = 4
let g:ycm_enable_diagnostic_highlighting = 0
set completeopt-=preview
let g:ycm_add_preview_to_completeopt = 0

" Asynchronous Lint Engine
let g:ale_sign_column_always = 1

" Command binding
let mapleader = ","
nnoremap <Leader>r :%s///g<Left><Left>

nnoremap <C-p> :Files<CR>
nnoremap <C-b> :Buffers<CR>
nnoremap <C-f> :Rg<CR>
nnoremap <C-t> :TestFile<CR>

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
