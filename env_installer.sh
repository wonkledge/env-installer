#!/bin/bash

#update debian/ubuntu package manager
sudo apt-get update && sudo apt-get upgrade

#install global tools
sudo apt-get install -y git curl net-tools
sudo apt-get install -y python-software-properties
sudo apt-get install -y apt-transport-https ca-certificates gnupg-agent software-properties-common

#remove old vim, install fresh new one
sudo apt-get remove vim && sudo apt-get install vim
sudo apt-get install -y python3-dev cmake

#install vim-plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

#install nodejs
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash –
sudo apt-get install -y nodejs

#install docker
sudo apt-get remove docker docker-engine docker.io containerd runc
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

#install docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

#Create file
mkdir ~/.vim/colors
cp gruvbox.vim ~/.vim/colors
cp .vimrc ~
cp .gitconfig ~

vim -c PlugInstall

#Finish YouCompleteMe installation
cd .vim/plugged/YouCompleteMe && ./install.sh --tern-completer

